import Vue from 'vue'
import VueRouter from 'vue-router'
import MechanismManage from '../Views/MechanismManage'
import MechanismDetails from '../Views/MechanismDetails'
import AddMechanism from '../Views/AddMechanism'
import Index from '../views/Index'
import ListMechanismcontact from '../Views/ListMechanismcontact'
import Home from '../components/Home'


/*import HelloWorld from '@/components/HelloWorld'
import { component } from 'vue/types/umd'*/

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Default',
    redirect: '/home',
    component: Home
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    show: true,
    redirect: '/ListMechanismcontact',
    children: [
      {
        path: '/ListMechanismcontact',
        name: '机构联系方式查询',
        component: ListMechanismcontact
      }
    ]
  },
  {
    path: '/',
    name: '机构信息管理',
    component: Index,
    show: true,
    redirect: '/MechanismManage',
    children: [//子页面
      {
        path: '/MechanismManage',
        name: '查询机构',
        component: MechanismManage
      },
      {
        path: '/MechanismDetails',
        name: '机构基本信息',
        component: MechanismDetails
      },
      {
        path: '/AddMechanism',
        name: '新增机构',
        component: AddMechanism
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

